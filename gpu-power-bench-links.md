

#### An article announcing vulkan's dav2d decoder
https://www.phoronix.com/scan.php?page=news_item&px=DAV1D-Vulkan-GLES-Experiment

#### Some maths, measures and graphs

[] https://hal-amu.archives-ouvertes.fr/hal-01770233/document
[] http://www.eecg.toronto.edu/~jayar/pubs/andargie/andargieAfricon15.pdf


#### But it doesn't talk about power benchmark
https://benchmarks.ul.com/support/how-to-benchmark

#### Only about computing perfs
https://web.wpi.edu/Pubs/E-project/Available/E-project-030212-123508/unrestricted/Benchmarking_Final.pdf
https://www.phoronix.com/scan.php?page=article&item=16-armlinux-sep2018&num=1


##### Most used gpu
https://deviceatlas.com/blog/most-used-smartphone-gpu


### nvidia :
https://cryptomining-blog.com/tag/nvidia-monitor-power-usage/
    > nvidia-smi

#### Librarr's documentation
https://developer.nvidia.com/nvidia-management-library-nvml


### They mesure gpu performance physically

https://hal.archives-ouvertes.fr/hal-00348672/document

```
To measure the power consumption we use a Tektronix TDS 3032 oscilloscope with
the built-in analogue 20 MHz low-pass filter. The first input measures the
current with a clamp sensor CA60, while the second measures voltage. The product
of both measurements gives us the power consumption of the GPU through the
external power. We took into consideration the power provided to the PCIe bus as
well as the external power of the GPU. We noticed during ourtests that the main
source of power is provided by the external link, as during computation only 10
to 15 extra W are coming from the PCI bus. We are aware that the measurements
collected using this methodology are subject to caution. First, we not only
measure the power consumption of the GPU but we also measure the power
consumption of the whole board that includes the GPU, the graphic memory, DC/DC
voltage converters, and othersICs (bridge, video chipset, ... ). However we
assume that we will notice a difference in the power consumption only if these
parts are stressed. Secondly, wemeasure the consumption a head of the board
voltage regulators which include
```


#### Others
https://ieeexplore.ieee.org/document/7348036
https://www.khronos.org/registry/vulkan/specs/1.0/html/vkspec.html#VkQueueFlagBits

